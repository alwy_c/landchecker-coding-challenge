## How to run

- Run `npm install`

- Create a `dev.js` file inside the `config` directory, and assign your MapBox API key as a string to the property `MAPBOX_API_KEY`

- The file should look something like this:

  ```
  export default {
  MAPBOX_API_KEY: <Your-API-Key-Here>
  };
  ```

- Then run `npm run start`

## Landchecker Coding Challenge

- This repo implements Landchecker's coding challenge (around 3.5/4 functionalities done).

- Clicking on a marker should display the property details.

- The application would always have 2 seconds delay because an "api call" was mocked.

## Reflection

- I would implement the tests next time I do this.

- I got stuck while implementing filtering. I know how to filter a layer using mapbox gl js, but having just learned react a few days ago, I am still wrapping my head around controlled vs uncontrolled components. I would have had mapbox's setFilter method update its array of filter values.

- I would use [react-map-gl](https://uber.github.io/react-map-gl/#/) and [formik](https://github.com/jaredpalmer/formik) the next time I do this
