import React, { Component } from 'react';
import mapboxgl from 'mapbox-gl';
import config from '../../config/dev';
import Map from './Map';

mapboxgl.accessToken = config.MAPBOX_API_KEY;

export default class App extends Component {
  render() {
    return <Map />;
  }
}
