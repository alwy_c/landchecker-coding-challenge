import React, { Component } from 'react';
import mapboxgl from 'mapbox-gl';

import config from '../../config/dev';
import api from '../utils/api';
import tools from '../utils/tools';
import Overlay from './overlay/Overlay';

mapboxgl.accessToken = config.MAPBOX_API_KEY;
const LAYER_NAME = 'locations';

export default class Map extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentLocation: '',
      councils: '',
      filterValues: []
    };

    this.showLocationDetail = this.showLocationDetail.bind(this);
    this.handleFilter = this.handleFilter.bind(this);
  }

  showLocationDetail(event) {
    // Query all the rendered points in the view
    let features = this.map.queryRenderedFeatures(event.point, {
      layers: [LAYER_NAME]
    });

    if (features.length) {
      let clickedPoint = features[0];
      this.setState({
        currentLocation: clickedPoint
      });
    }
  }

  handleFilter(council) {
    // FILTERING NOT DONE YET

    // const FILTER_EXPRESSION = [
    //   'match',
    //   ['downcase', ['get', 'council']],
    //   [clickedPoint.properties.council.toLowerCase(), 'cardinia'],
    //   true,
    //   false
    // ];

    // this.map.setFilter(LAYER_NAME, FILTER_EXPRESSION, {
    //   validate: true
    // });

    console.log('Filtering is not done yet!');
  }

  componentDidMount() {
    this.map = new mapboxgl.Map({
      container: this.mapContainer,
      center: [146.941079580425, -37.1029747328265],
      style: 'mapbox://styles/mapbox/streets-v9',
      zoom: 7.5
    });

    this.map.on('load', () => {
      api
        .fetchLocations()
        .then(locations => {
          const geoJsonLocations = tools.convertToGeoJson(locations);
          const councils = locations.map(locations => {
            return locations.council;
          });
          const uniqueCouncils = tools.getUniqueArray(councils);
          let filterValues = [];
          uniqueCouncils.forEach(council => {
            filterValues[council] = false;
          });

          this.setState({
            councils: uniqueCouncils,
            filterValues: filterValues
          });

          this.map
            .addSource('landchecker-data', {
              type: 'geojson',
              data: {
                type: 'FeatureCollection',
                features: geoJsonLocations
              }
            })
            .addLayer({
              id: LAYER_NAME,
              type: 'symbol',
              source: 'landchecker-data',
              layout: {
                'icon-image': 'circle-15',
                'icon-allow-overlap': true
              }
            });

          // Center the map on the coordinates of any clicked symbol from the 'locations' layer.
          this.map.on('click', LAYER_NAME, this.showLocationDetail);

          // Change the cursor to a pointer when the it enters a feature in the 'locations' layer.
          this.map.on('mouseenter', LAYER_NAME, () => {
            this.map.getCanvas().style.cursor = 'pointer';
          });

          // Change it back to a pointer when it leaves.
          this.map.on('mouseleave', LAYER_NAME, () => {
            this.map.getCanvas().style.cursor = '';
          });
        })
        .catch(error => {
          console.error(error);
        });
    });
  }

  componentWillUnmount() {
    this.map.remove();
  }

  render() {
    const style = {
      position: 'absolute',
      top: 0,
      bottom: 0,
      width: '100%'
    };

    return (
      <div className="map">
        <div style={style} ref={el => (this.mapContainer = el)} />
        <Overlay
          currentLocation={this.state.currentLocation}
          councils={this.state.councils}
          handleFilter={this.handleFilter}
          filterValues={this.state.filterValues}
        />
      </div>
    );
  }
}
