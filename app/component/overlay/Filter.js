import React, { Component } from 'react';

export default class Filter extends Component {
  render() {
    return (
      <div className="overlay council-filter">
        <form>
          {this.props.councils ? (
            <div className="filter-list">
              {this.props.councils.map(council => {
                return (
                  <div key={council} className="filter-item">
                    <input
                      type="checkbox"
                      onChange={this.props.handleFilter}
                      checked={this.props.filterValues[council]}
                      id={council}
                    />
                    <label htmlFor={council}>{council}</label>
                  </div>
                );
              })}
            </div>
          ) : (
            <h3>Loading Data</h3>
          )}
        </form>
      </div>
    );
  }
}
