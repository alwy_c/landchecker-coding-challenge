import React, { Component } from 'react';

export default class LocationDetail extends Component {
  render() {
    const location = this.props.currentLocation;
    return location ? (
      <div>
        <h3>Property ID: {location.properties.property_id}</h3>
        <h3>Council: {location.properties.council}</h3>
        <h3>
          Council Property Number: {location.properties.council_property_number}
        </h3>
        <h3>Full Address: {location.properties.full_address}</h3>
        <h3>Local Government Area Code: {location.properties.lga_code}</h3>
      </div>
    ) : (
      <h2>Click on a location on the map.</h2>
    );
  }
}
