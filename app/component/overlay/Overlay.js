import React, { Component } from 'react';

import LocationDetail from './LocationDetail';
import Filter from './Filter';

export default class Overlay extends Component {
  render() {
    return (
      <div>
        <Filter
          councils={this.props.councils}
          handleFilter={this.props.handleFilter}
          filterValues={this.props.filterValues}
        />
        <div className="overlay location-detail">
          <LocationDetail currentLocation={this.props.currentLocation} />
        </div>
      </div>
    );
  }
}
