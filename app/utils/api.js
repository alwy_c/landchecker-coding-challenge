import properties from '../../config/properties.json';

export default {
  // I put a delay on fetchLocation to simulate a late data return.
  fetchLocations: () => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        if (properties) {
          resolve(properties);
        } else {
          reject();
        }
      }, 2000);
    });
  }
};
