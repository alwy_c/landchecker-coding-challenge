export default {
  getUniqueArray: nonUniqueArray => {
    let uniqueArray = [...new Set(nonUniqueArray)];
    return uniqueArray;
  },

  convertToGeoJson(landcheckerJson) {
    const newArrayLand = landcheckerJson.map(location => {
      return {
        type: 'Feature',
        properties: {
          property_id: location.property_id,
          council: location.council,
          council_property_number: location.council_property_number,
          full_address: location.full_address,
          lga_code: location.lga_code,
          postcode: location.postcode
        },
        geometry: {
          type: 'Point',
          coordinates: [location.longitude, location.latitude]
        }
      };
    });

    return newArrayLand;
  }
};
